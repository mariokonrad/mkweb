#!/bin/bash -eu

## GCC-7
#export CMAKE_CXX_COMPILER=${HOME}/tmp/gcc-7/bin/g++
#export LD_LIBRARY_PATH=${HOME}/tmp/gcc-7/lib64

# GCC-6
export CMAKE_CXX_COMPILER=g++-6

